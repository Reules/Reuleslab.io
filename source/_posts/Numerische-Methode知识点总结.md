---
title: Numerische Methode知识点总结
date: 2018-03-07 19:35:46
tags:
---

  1. **Gauß-Algorithmus:** 高斯消去法，通常为线性方程求解。 又分为换行消元和不换行消元
  2. **LR-Zerlegung:** 将一个矩阵A分解为主对角线元素为1的下三角矩阵L和一个上三角矩阵R的乘积。这样便可以将对原来矩阵A的求解转换为对L,R的求解，更加容易实现。
  3. **Cholesky-Zerlegung:** 和LR-Zerlegung类似，这里将原矩阵A拆解为L和L的逆矩阵之积。
  4. **von-Mises/Wielandt Iteration:** 迭代法求近似特征值。 
  5. **Simplex-Algorithmus:**  利用矩阵转化求线性规划最优解。
  6. **Konditionszahl:**  线性方程Ax=b, 给出了数值b的精确度，根据条件数得到一个解x有多不精确的上限。
  7. **Newton-Verfahren:** 利用迭代方程逼近函数的某个零点(Nullstelle)。
  8. **Quadratur:**  近似求积分方法，逼近准确值。常见的有[Trapezregel][1], [Simpsonregel][2]等。
  9. **Anfangswertprobleme:**  给出函数$y(x)$的倒数和初始值$y(x_0)$，便可以通过迭代的方法求出$y(x_n)$的值。方法有 **Euler-Verfahren**, **Halbschrittverfahren**，**Rung-Kutta-Verfahren** 等等。
  10. **Randwertprobleme:**  给出函数$f(x)$的两个边界值$y(a)$和$y(b)$，根据函数$f(x)$的一阶导数和二阶导数的近似公式，可以得到任意的$y(x_n)$值。