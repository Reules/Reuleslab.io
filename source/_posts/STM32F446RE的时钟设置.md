---
title: STM32F446RE的时钟设置
date: 2018-03-05 20:06:17
tags: 嵌入式
---

---



stm32f446re微处理器提供了五种时钟源:

> * HSE: 高速外部时钟
> * LSE: 低速外部时钟
> * HSI: 高速内部时钟
> * LSI: 低速内部时钟
> * VCO(PLL): 锁相环内部的压控振荡器



---

**STM32F446RE时钟配置:**

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/159988823@N04/40594321052/in/dateposted-public/" title="stm32f446r_clock"><img src="https://farm5.staticflickr.com/4609/40594321052_0140a835f3.jpg" width="820" height="434" alt="stm32f446r_clock"></a>

---


>HSE的振源来自于外部的压控(温控)晶体振荡器(VXCO, TXCO), 其特点是精确和稳定，频率范围在4-26 MHz。
>LSE的振源来自于外部的晶体振荡器, 晶振的频率为32.768 KHz, 为其提供32 KHz的频率。
>HSI由内部的RC振荡电路提供16 MHz的频率。
>LSI由内部的RC振荡电路提供32 KHz频率。
>PLL内VCO的频率范围在100到432 MHz。



---

其实PLL我更愿意叫它频率合成器，在这里有一个倍频的作用，将外部频率加倍后作为时钟信号通过AHB和APB总线传给外设。
Analog Devices关于PLL的这篇文章写的很好，下面是链接:
[PLL基础][2]
[Direct digital synthesizer][3]
上图中还有一些分频器，如何实现可参考维基百科
[分频器][4]

[2]: http://www.analog.com/media/en/training-seminars/tutorials/MT-086.pdf
[3]: https://en.wikipedia.org/wiki/Direct_digital_synthesizer
[4]: https://zh.wikipedia.org/wiki/%E5%88%86%E9%A2%91%E5%99%A8