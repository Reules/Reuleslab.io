---
title: gcc寻找头文件机制
date: 2018-03-04 11:40:31
tags:
---

​	这两天一直忙于复习Numerische Methode的考试，时间不多。所以仅仅尝试一下图片插入功能，看能否成功。



<a data-flickr-embed="true"  href="https://www.flickr.com/photos/159988823@N04/25741008727/in/dateposted-public/" title="gcc头文件寻找机制"><img src="https://farm5.staticflickr.com/4719/25741008727_988b5a0607_b.jpg" width="1024" height="701" alt="gcc头文件寻找机制"></a>

------

​	备注:  其实gcc先找 -I 参数包含目录还是先找源文件所在文件夹我还不确定，下次查了再来改正。图片插入可以参考这篇博客:

[Hexo博客搭建之在文章中插入图片](https://yanyinhong.github.io/2017/05/02/How-to-insert-image-in-hexo-post/)

